 #!/usr/local/bin/perl -w
 
package SizeCheck;
use File::Listing;

use ComparisonToken;
use SizeToken;
use SpecifierToken;
use NumericToken;

# class that validates only specifier tokens

$name = '';
$comparator = '';
$value = -1;
$sizetoken = '';

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my $s = $_[1];
	my @tokens = split(/\s+/,$s);
	my $token_count = @tokens;
	
	#print ("String \"$s\" splits into $token_count tokens\n");
	
	if (!$token_count or $token_count < 5) {
#		print ("Not a size assertion\n");
		return 0;
	}
	
	$st = new SizeToken;
	$val = pop(@tokens);
	if ($st->isValid($val)) {
		$sizetoken = $val;
		
		$nt = new NumericToken;
		$val = pop(@tokens);
		if ($nt->isValid($val)) {
			$value = $val;
	
			$ct = new ComparisonToken;
			$val = pop(@tokens);
			if ($ct->isValid($val)) {
				$comparator = $val;
			
				$st = new SpecifierToken;
				$val = join(' ', @tokens);
				#print ("is specifier $val valid?\n");
				if ($st->isValid($val)) {
					$name = $val;
					if (lc($name) ne "any file") {
						shift(@tokens);
						shift(@tokens);
						$name = join(' ', @tokens);
					}
					return 1;
				} 
			} else {
				$val = pop(@tokens).' '.$val;
				if ($ct->isValid($val)) {
					$comparator = $val;
					
					$st = new SpecifierToken;
					$val = join(' ', @tokens);
					if ($st->isValid($val)) {
						$name = $val;
						if (lc($name) ne "any file") {
							shift(@tokens);
							shift(@tokens);
							$name = join(' ', @tokens);
						}
						return 1;
					} else {
						print ("Invalid specifier string\n");
					}			
				} else {
					print ("Invalid comparison string\n");
				}
			}
		} else {
			print ("Invalid size value\n");
		}
	} else {
		print ("Invalid size token\n");
	}
	return 0;
}

sub run {
	
	my ($self, $path, $mask) = @_;
	
	$st = new SpecifierToken;
	#print("Specifier name is $name\n");
	
	if (lc($name) eq "any file") { # check to see if any file meets the condition
		#print "this is an \"any file\" case\n";
		
		# we must iterate through all files in the folder
		foreach $file (parse_dir(`ls -al $path`)) {
			
			my ($filename, $type, $size, $mtime, $mode) = @$file;
			
			# resolve the numerical value with the size token ie 2 kb = 2048
			if ($type ne 'd') {
				#print ("file: $filename, $type, $size, $mtime, $mode\n");
				$resolvedval = resolveFileSize($value, $sizetoken);
				#print("comparison value is $value $sizetoken | $size $comparator $resolvedval\n");
				# if this comparison is true, return true, else keep looping
				if (compare($size, $resolvedval)) {
					#print ("found one that compared favorably");
					return 1;
				}
			}	
		}
		
		return 0;
		
	} else { # check to see if the named file meets the condition
		if ($name =~ /\*/) {
			print ("grammer name contains wildcard\n");
			foreach $file (parse_dir(`ls -al $path/$name`)) {

				my ($filename, $type, $size, $mtime, $mode) = @$file;
				
				$size = -s $filename;
			
				# resolve the numerical value with the size token ie. 2 kb = 2048
				$resolvedval = resolveFileSize($value, $sizetoken);
				#print("comparison value is $value $sizetoken | $size $comparator $resolvedval\n");
				# compare the two sizes according to the defined comparison token
				if (compare($size, $resolvedval)) {
					return 1;
				}
			}
            return 0;
            
		} else {
			unless (-e "$path/$name") {
				print ("file does not exist\n");
				return 0;
			}
			
			$size = -s $name;
			
			# resolve the numerical value with the size token ie. 2 kb = 2048
			$resolvedval = resolveFileSize($value, $sizetoken);
			#print("comparison value is $value $sizetoken | $size $comparator $resolvedval\n");
			# compare the two sizes according to the defined comparison token
			return compare($size, $resolvedval)
		}
	}
	
}

# convert the value and size token into a single integer value ie 2 kb = 2048
sub resolveFileSize {
	my $val = $_[0];
	my $token = lc($_[1]);
	$token =~ s/^\s+|\s+$//g;
	my $st = new SizeToken;
	
	if ($token eq "kb") {
		return $val * (2**10); 
	} elsif ($token eq "mb") {
		return $val * (2**20); 
	} elsif ($token eq "gb") {
		return $val * (2**30); 
	} elsif ($token eq "tb") {
		return $val * (2**40); 
	} elsif ($token eq "pb") {
		return $val * (2**50); 
	} elsif ($token eq "xb") {
		return $val * (2**60); 
	} elsif ($token eq "b" or $token eq "bytes") {
		return $val;
	} else {
		return $val;
	}
	
}

sub compare {
	my $x = $_[0];
	my $y = $_[1];
	
	$comparator = lc($comparator);
	
	$ct = new ComparatorToken;

	if (($comparator eq "less than") or ($comparator eq "fewer than") or ($comparator eq '<')) {
		return $x < $y;
	} elsif (($comparator eq "greater than") or ($comparator eq "more than") or ($comparator eq '>')) {
		return $x > $y;
	} elsif (($comparator eq "at most") or ($comparator eq '<=')) {
		return $x <= $y;
	} elsif (($comparator eq "at least") or ($comparator eq '>=')) {
		return $x >= $y;
	} elsif (($comparator eq "exactly") or ($comparator eq "equal to") or ($comparator eq "equals") or ($comparator eq '=')) {
		return $x == $y;
	} else {
		return 0;
	}
}
1;