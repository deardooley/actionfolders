 #!/usr/local/bin/perl -w
 
package ExistenceCheck;

use File::Listing;
use CrudToken;
use SpecifierToken;

# class that performs existence checks

$grammer_file_name = '';
$grammer_operation = '';

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	
	my ($self, $s) = @_;
	
	@tokens = split(/\s+/,$s);
	$token_count = @tokens;
	
	if ($token_count > 4 or $token_count < 3 or $s =~ /any file contains /i or $s =~ /file named * contains /i) {
#		print ("Not an existence assertion\n");
		return 0;
	}
	
	$ct = new CrudToken;
	$val = pop(@tokens);
	if ($ct->isValid($val)) {
		$grammer_operation = $val;
		$grammer_operation =~ s/^\s+|\s+$//g; # trim the operation
		
		$st = new SpecifierToken;
		$val = join(' ', @tokens);
		
		if ($st->isValid($val)) {
			$grammer_file_name = $val;

			# if the specifier is not an 'any file' denotation, then get
			# the file name by splitting on the 'named' keyword and 
			# taking the second token.
			
			if (lc($grammer_file_name) ne "any file") {
				@spectokens = split(/named/,$grammer_file_name);
				$grammer_file_name = $spectokens[1];
				$grammer_file_name =~ s/^\s+|\s+$//g;
			}
			return 1;
		} else {
			print ("Invalid specifier value\n");
		}
	} else {
		print ("Invalid crud token\n");
	}
	
	return 0;
}

sub run {
	
	my ($self, $path, $event_mask, $event_file) = @_;
	
	$st = new SpecifierToken;
	
	if (lc($grammer_file_name) eq "any file") { # check to see if any file meets the condition
	
		# we must iterate through all files in the folder
		foreach $file (parse_dir(`ls -al $path`)) {
			my ($name, $type, $size, $mtime, $mode) = @$file;
			
			# if this comparison is true, return true, else keep looping
			if (checkExistence($name, $grammer_operation, $path, $event_mask, $event_file)) {
				return 1;
			}	
		}
		
		return 0;
		
	} else { # check to see if the named file meets the condition
		if ($grammer_file_name =~ /\*/) {
			print ("grammer name contains wildcard\n");
			foreach $file (parse_dir(`ls -al $path/$grammer_file_name`)) {

				my ($name, $type, $size, $mtime, $mode) = @$file;
				#@filepath = split("/",$name);
				#print ("checking file $path[@path - 1]\n");
				# if this comparison is true, return true, else keep looping
                if (checkExistence($name, $grammer_operation, $path, $event_mask, $event_file)) {
                	return 1;
                }
            }

            return 0;
		} else {
			return checkExistence($grammer_file_name, $grammer_operation, $path, $event_mask, $event_file);
		}
	}
	
}

# convert the value and size token into a single integer value ie 2 kb = 2048
sub checkExistence {
	my $userFile = $_[0];
	my $operation = $_[1];
	my $path = $_[2];
	my $event_mask = $_[3];
	my $event_file = $_[4];
	
	$ct = new CrudToken;
	
	if (lc($operation) eq "exist" or lc($operation) eq "exists") {
		if (glob ("$path/$userFile")) {
			return 1;
		} else {
			return 0;
		}
	} elsif (lc($operation) eq "!exist" or lc($operation) eq "!exists") {
		if (glob ("$path/$userFile")) {
			return 0;
		} else {
			return 1;
		}
	} elsif (lc($operation) eq "created") {
		print "$userFile eq $event_file and $event_mask eq create_event\n";
		return ("$path/$userFile" eq $event_file and $event_mask eq "create_event"); # we need the actual event code to find out if the file was just created
	} elsif (lc($operation) eq "modified") {
		return ("$path/$userFile" eq $event_file and $event_mask eq "modify_event");
		#return -M $userFile > 1; # we need the actual event code to find out if the file was just modified
	} elsif (lc($operation) eq "deleted") {
		return ("$path/$userFile" eq $event_file and $event_mask eq "delete_event");
		#return !(-e $userFile); # we need the actual event code to find out if the file was just created
	} else {
		return 0;
	}
	
}

1;
