 #!/usr/local/bin/perl -w
 
package ComparisonToken;

$GT = "greater than";
$MT = "more than";
$GTE = "at least";
$EQ = "equal to";
$EQS = "equals";
$EX = "exactly";
$LTE = "at most";
$LT = "less than";
$FT = "fewer than";

# class that validates numeric tokens

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	return ($s =~ /$GT/i or
			$s =~ /$MT/i or
			$s =~ /$GTE/i or
			$s =~ /$EQ/i or
			$s =~ /$EQS/i or 
			$s =~ /$EX/i or
			$s =~ /$LTE/i or
			$s =~ /$LT/i or
			$s =~ /$FT/i or
			$s =~ /\</i or
			$s =~ /\<\=/i or
			$s =~ /\=/i or
			$s =~ /\>/i or
			$s =~ /\>\=/i);
}

1;