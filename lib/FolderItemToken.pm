 #!/usr/local/bin/perl -w
 
package FolderItemToken;

$ITEM 		= "item";
$ITEMS		= "items";
$FILE 		= "files";
$FILES		= "file";
$FOLDER 	= "folder";
$FOLDERS	= "folders";

# class that validates numeric tokens

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	return ($s =~ /$ITEM/i or 
			$s =~ /$ITEMS/i or 
			$s =~ /$FILE/i or 
			$s =~ /$FILES/i or 
			$s =~ /$FOLDER/i or 
			$s =~ /$FOLDERS/i);
}

1;