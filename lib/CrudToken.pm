 #!/usr/local/bin/perl -w
 
package CrudToken;

$CREATED 			= "created";
$MODIFIED 			= "modified";
$DELETED 			= "deleted";
$EXIST 			= "exist";
$EXISTS 			= "exists";
$DOES_NOT_EXIST		= "!exist";
$DOES_NOT_EXISTS	= "!exists";

# class that validates numeric tokens

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	return (lc($s) eq $CREATED or
			lc($s) eq $MODIFIED or
			lc($s) eq $DELETED or
			lc($s) eq $EXIST or 
			lc($s) eq $EXISTS or 
			lc($s) eq $DOES_NOT_EXIST or
			lc($s) eq $DOES_NOT_EXISTS);
}

1;