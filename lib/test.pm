 #!/usr/local/bin/perl -w

use DirectoryCheck;
use ExistenceCheck;
use GrepCheck;
use JobCheck;
use ProcessCheck;
use SizeCheck;

my $checker;
$dirCheck = new DirectoryCheck;
$existCheck = new ExistenceCheck;
$grepCheck = new GrepCheck;
$jobCheck = new JobCheck;
$procCheck = new ProcessCheck;
$sizeCheck = new SizeCheck;
my $s= '';

do {
	print "Please enter assertion: \n";
	$s = <STDIN>;
	chomp $s;
	$valid = 'false';
	$result = 'false';
	$checker = validate($s);
	if ($checker) {
		if (run($checker)) {
			$result = 'true';
		} 
		$valid = 'true';
	}
	print ("Grammer is valid: $valid\n");
	print ("Assertion is valid: $result\n");
} while ($s);


sub validate {
	my $val = shift;
	
	if ($dirCheck->isValid($val)) {
		$checker = $dirCheck;
		$type = "directory";
	} elsif ($procCheck->isValid($val)) {
		$checker = $procCheck;
		$type = "process";
	} elsif ($jobCheck->isValid($val)) {
		$checker = $jobCheck;
		$type = "job";
	} elsif ($existCheck->isValid($val)) {
		$checker = $existCheck;
		$type = "existence";
	} elsif ($sizeCheck->isValid($val)) {
		$checker = $sizeCheck;
		$type = "size";
	} elsif ($grepCheck->isValid($val)) {
		$checker = $grepCheck;
		$type = "grep";
	} else {
		undef $type;
		undef $checker;
	}
	
	if (! $type) {
		return 0;
	} else {
		print (ucfirst($type)." assertion was made\n");
		return $checker;
	}
}

sub run {
	my $run_checker = shift;
	return $run_checker->run();
}

