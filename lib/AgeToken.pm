 #!/usr/local/bin/perl -w
 
package AgeToken;

$YEAR = 'year';
$YEARS = 'years';
$MONTH = 'month';
$MONTHS = 'months';
$DAY = 'day';
$DAYS = 'days';
$HOUR = 'hour';
$HOURS = 'hours';
$MINUTE = 'minute';
$MINUTES = 'minutes';
$SECOND = 'second';
$SECONDS = 'seconds';

# class that validates numeric tokens

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	return ($s =~ /$YEAR/i or 
			$s =~ /$YEARS/i or 
			$s =~ /$MONTH/i or 
			$s =~ /$MONTHS/i or 
			$s =~ /$DAY/i or 
			$s =~ /$DAYS/i or 
			$s =~ /$HOUR/i or 
			$s =~ /$HOURS/i or
			$s =~ /$MINUTE/i or
			$s =~ /$MINUTES/i or
			$s =~ /$SECOND/i or
			$s =~ /$SECONDS/i);
	
}

1;