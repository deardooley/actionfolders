#!/usr/local/bin/perl -w
 
package GrammerParser;

$VERSION = 1.00;
our @ISA     = qw(Exporter);
our @EXPORT_OK = qw(processTemplate);
our %EXPORT_TAGS = ( DEFAULT => [qw(&processTemplate)],
				 BOTH	=> [qw(&processTemplate)]);
				 
use DirectoryCheck;
use ExistenceCheck;
use GrepCheck;
use JobCheck;
use ProcessCheck;
use SizeCheck;
use AgeCheck;

$dirCheck = new DirectoryCheck;
$existCheck = new ExistenceCheck;
$grepCheck = new GrepCheck;
$jobCheck = new JobCheck;
$procCheck = new ProcessCheck;
$sizeCheck = new SizeCheck;
$ageCheck = new AgeCheck;

sub processTemplate {
	my $path = shift;
	my $event_mask = shift;
	my $event_file = shift;
	my $started_grammer = 0;
	my $all_valid = 1;
	
	open (ACTIONTEMPLATE, "$path/.action") or print ("failed to open file\n");
	
	while (<ACTIONTEMPLATE>) {
		chomp $_;
		
		# ignore hash comments either in line or separate
		if ($_ =~ /\#/) {
			@hashsplit = split("\#",$_);
			$_ = $hashsplit[0];
		}
		
#		print "$_\n";
		
		if ($started_grammer == 0) {
			if ($_ =~ /DEPENDENCIES/i) {
				$started_grammer = 1;
			} 
#			elsif ($_ =~ /ACTIONS/i) {
#				$started = 0;
#			} 
		} else {
			
			if (!$_) { # ignore blank lines
				next;	
			} elsif ($_ =~ /ACTIONS/i) {
				$started_grammer = 0;
				last;
			} 
			
			my $checker = validate($_);
			if ($checker) {
				if (testAssertion($checker, $path, $event_mask, $event_file)) {
					print ("Assertion \"$_\" is true\n");
				} else {
					$all_valid = 0;
					print ("Assertion \"$_\" is false\n");
					last;
				}
				#print ("Grammer \"$_\" is valid\n");
			} else {
				$all_valid = 0;
				print ("Grammer \"$_\" is invalid\n");
				last;
			}
		}
	}
	
	close ACTIONTEMPLATE;
	
	print "All tests passed: $all_valid\n";
	if ($all_valid == 1) {
		# process the user-defined actions here
		performActions("$path", $event_file);
	}
}

sub performActions {
	$abspath = shift;
	$event_file = shift;
	# this can lead to an infinite loop when the ACTIONS script code modifies
	# files in the action folder. Need a way to block the processing of other events
	# until this completes or to detect infinite loops.
	system "export EVENT_FILE=$event_file; cd $abspath; awk '/ACTIONS/,EOF' \"$abspath/.action\" | grep -vw ACTIONS | /bin/sh";
}

sub validate {
	my $val = shift;
	
	if ($dirCheck->isValid($val)) {
		$checker = $dirCheck;
		$type = "directory";
	} elsif ($procCheck->isValid($val)) {
		$checker = $procCheck;
		$type = "process";
	} elsif ($jobCheck->isValid($val)) {
		$checker = $jobCheck;
		$type = "job";
	} elsif ($existCheck->isValid($val)) {
		$checker = $existCheck;
		$type = "existence";
	} elsif ($ageCheck->isValid($val)) {
		$checker = $ageCheck;
		$type = "age";
	} elsif ($sizeCheck->isValid($val)) {
		$checker = $sizeCheck;
		$type = "size";
	} elsif ($grepCheck->isValid($val)) {
		$checker = $grepCheck;
		$type = "grep";
	} else {
		undef $type;
		undef $checker;
	}
	
	if (! $type) {
		return 0;
	} else {
#		print (ucfirst($type)." assertion was made\n");
		return $checker;
	}
}

sub testAssertion {
	my $run_checker = shift;
	my $path = shift;
	my $event_mask = shift;
	my $event_file = shift;
	
	return $run_checker->run($path, $event_mask, $event_file);
}

#processTemplate('/Users/dooley/workspace/ActionFolders/samples','create_event', 'hurricane-isabel.jpg');

1;
