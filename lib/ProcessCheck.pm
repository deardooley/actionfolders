 #!/usr/local/bin/perl -w
 
package ProcessCheck;

use NumericToken;
use ExistenceToken;

# class that validates only specifier tokens

$exists = 0;
$processid = -1;

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	@tokens = split(/ /,$s);
	$token_count = @tokens;
	if ($token_count != 3) {
#		print ("Not a process assertion\n");
		return 0;
	}
	
	if ($tokens[0] =~ /process/i) {
		$nt = new NumericToken;
		if ($nt->isValid($tokens[1])) {
			$processid = $tokens[1];
			$et = new ExistenceToken();
			if ($et->isValid($tokens[2])) {
				$exists = (lc($tokens[2]) eq "exists");
				return 1;
			} else {
				print("Invalid status\n");
			} 
		} else {
			print("Invalid process id\n");
		}
	} else {
		print("Not a process status assertion\n");
	}
	return 0;
}

sub run {
	# exec the system call to query for process status information
	# resulting format is null or similar to:
	# 29626 ?        Ssl    0:05 sshd: bxu@notty   
	# because we grep by processid, there will only be one line returned
	open (PSTEST, "ps -ef $processid | grep $processid | grep -v grep |");
	while (<PSTEST>) {
		$result = $_;
	}
	#$result = <PSTEST>;
	
	close PSTEST;
	$et = new ExistenceToken;
	
	if ($result) { # an entry was found for the process
		undef $result;
		
		# if the user was checking for existence, return true, else false
		return $exists;
	} else { # no result found, so the process does not exist
		undef $result;
		
		# if the user was checking for non existence, return true, else false
		return !$exists;
	}
}

1;