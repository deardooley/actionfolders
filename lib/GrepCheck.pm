 #!/usr/local/bin/perl -w
 
package GrepCheck;

use SpecifierToken;
use AlphaNumericToken;
use Cwd;

# class that validates only grep expressions

$searchstring = '';
$name = '';

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my $s = $_[1];
	@tokens = split(/\s+contains\s+/,$s);
	$token_count = @tokens;
	
	if ($token_count < 2) {
#		print ("Not a grep assertion\n");
		return 0;
	}
	
	$st = new SpecifierToken;
	#print ("is specifier \"$tokens[0]\" valid?\n");
	if ($st->isValid($tokens[0])) {
		$name = $tokens[0];
		$name =~ s/^\s+|\s+$//g; # trim the name
		
		# if the specifier is not an 'any file' denotation, then get
		# the file name by splitting on the 'named' keyword and 
		# taking the second token.
		if (lc($name) ne "any file") {
			@spectokens = split(/named/,$name);
			$name = $spectokens[1];
			$name =~ s/^\s+|\s+$//g;
		}
		
		$at = new AlphaNumericToken;
		if ($at->isValid($tokens[1])) {
			$searchstring = $tokens[1];
			$searchstring =~ s/^\s+|\s+$//g;
			return 1;
		} else {
			print ("Invalid search string\n");
		}
	} else {
		print ("Invalid specifier token\n");
	}
	
	return 0;
}

sub run {
	
	my ($self, $path, $mask) = @_;
	
	$st = new SpecifierToken;
	
	if (lc($name) eq "any file") {
		
		open (GREPTEST, "grep -r \"$searchstring\" $path |");
		$result = <GREPTEST>;
		close GREPTEST;
		
		if ($result) {
			return 1;
		} else {
			return 0;
		}	
	} else {
		if (!($name =~ /\*/)) {
			unless (-e "$path/$name") {
				#print ("file does not exist\n");
				return 0;
			}
		}
		
		open (GREPTEST, "grep \"$searchstring\" $path/$name |");
		$result = <GREPTEST>;
		close GREPTEST;
		
		if ($result) {
			return 1;
		} else {
			return 0;
		}	
	}
}

1;