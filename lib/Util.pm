 #!/usr/local/bin/perl -w

## Utility module for random string and file system tasks

## Trim function
sub trim {
   my $string = shift;
   $string =~ s/^\s+|\s+$//g;
   return $string;
}