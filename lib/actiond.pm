#!/usr/local/bin/perl -w

# This is the main executable script for the action folder daemon.
# At startup, it scans the user's home space for .action files, creating
# watches for every folder. When a new .action file is created, a new watch is added. 
# .action folder events result in the invoking of the GrammerParser module that 
# processes the .action file DEPENDENCIES section and, if valid and true, 
# executes the script in the ACTIONS section. 

BEGIN { push @INC, "$0/../" }

use Linux::Inotify2;
use File::Find;
use Event;

system "echo $$ > ../pid";

my $inotify = new Linux::Inotify2 
	or die "Unable to create new inotify object: $!"; 

 # for Event:
 Event->io (fd =>$inotify->fileno, poll => 'r', cb => sub { $inotify->poll });

 # look for the user-defined environmental variable for the search base
 $watch_root = $ENV{'ACTION_FOLDER_HOME'};
 
 if (!$watch_root) { # otherwise use their home folder.
 	$watch_root = $ENV{'HOME'};
 }
 
 find(\&wanted, $watch_root);
 
 Event::loop;
  
sub wanted {
 	#print ("Examining item: $File::Find::name\n");
 	
 	if (-d $File::Find::name) { 
 		
		# if the current file is a directory add a watch to it
 		$inotify->watch ($File::Find::name, IN_CREATE | IN_CLOSE_WRITE | IN_DELETE | IN_DELETE_SELF, \&performAction);
 		
 		print("registering folder $File::Find::name\n");
 		return;
 	}
 }

sub performAction {
	use GrammerParser qw(:DEFAULT);
	my $e = shift; # the thrown event
	$path = substr($e->fullname,0,index($e->fullname,$e->name)-1); # the watch directory
	$event_file = $e->fullname;
	
	#print ("watch path is ".$e->{w}{name}."\n");
	print ("event path is ".$event_file."\n");
	if ($e->IN_DELETE and $event_file eq $e->{w}{name}) { # check to see if the watch folder was just deleted, if so cancel the watch 

		print ("folder ".$e->{w}{name}." was deleted. canceling watch\n");		    			
		$e->w->cancel;
		    			
	} elsif (-e $e->{w}{name}."/.action" # only process actions for folders with .action files in them
			and $e->{w}{name}."/.action" ne $e->fullname) { # ignore modifications to .action files
		 				
    	if ($e->IN_MODIFY) { # watch all file/folder modification
    		
    		GrammerParser::processTemplate($path, 'modify_event', $event_file);		
    	
    	} elsif ($e->IN_DELETE) { # watch all file/folder deletions
    		
    		GrammerParser::processTemplate($path, 'delete_event', $event_file);
    	
    	} elsif ($e->IN_CREATE) { # watch all file/folder creations
		    		
	   		if (-d $event_file) {
	   			# new folder was created, register it
	   			print("registering folder $event_file\n");
	    		$inotify->watch ($event_file, IN_CREATE | IN_CLOSE_WRITE | IN_DELETE, \&performAction);
	   		}
		    		
		  	GrammerParser::processTemplate($path, 'create_event', $event_file);
		}
#	} else {
#		print ("took no action\n");
	}
	
#	if ($e->IN_ACCESS) {print $e->fullname." was accessed\n";}
#	if ($e->IN_MODIFY) {print $e->fullname." was modified\n";}
#	if ($e->IN_ATTRIB) {print $e->fullname." was meta modified\n";}
#	if ($e->IN_CLOSE_WRITE) {print $e->fullname." was closed with write\n";}
#	if ($e->IN_CLOSE_NOWRITE) {print $e->fullname." was closed without writing\n";}
#	if ($e->IN_OPEN) {print $e->fullname." was opened\n";}
#	if ($e->IN_MOVED_FROM) {print $e->fullname." was moved from\n";}
#	if ($e->IN_MOVED_TO) {print $e->fullname." was moved to\n";}
#	if ($e->IN_CREATE) {print $e->fullname." was created\n";}
#	if ($e->IN_DELETE) {print $e->fullname." had a file/folder deleted\n";}
#	if ($e->IN_DELETE_SELF) {print $e->fullname." was itself deleted\n";}
#	if ($e->IN_MOVE_SELF) {print $e->fullname." was itself moved\n";}
#	if ($e->IN_ALL_EVENTS) {print $e->fullname." had an event\n";}
#	if ($e->IN_ONESHOT) {print $e->fullname." was oneshot\n";}
#	if ($e->IN_ONLYDIR) {print $e->fullname." was only watched b/c it was a directory\n";}
#	if ($e->IN_DONT_FOLLOW) {print $e->fullname." was not following a sym link\n";}
#	if ($e->IN_MASK_ADD) {print $e->fullname." was triggered by an unsupported event\n";}
#	if ($e->IN_CLOSE) {print $e->fullname." was closed undetermined\n";}
#	if ($e->IN_MOVE) {print $e->fullname." was moved undetermined\n";}
		
	
}