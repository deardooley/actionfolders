 #!/usr/local/bin/perl -w
 
package DirectoryCheck;

use File::Listing;
use ComparisonToken;
use FolderItemToken;
use NumericToken;

# class that validates only specifier tokens


sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	@tokens = split(/\s+/,$s);
	$token_count = @tokens;
	
	if ($token_count < 5) {
#		print("Not a directory assertion\n");
		return 0;
	}
	
	if ($s =~ /^folder contains/i) {
		$ct = new ComparisonToken;
		$val = $tokens[2];
		if ($ct->isValid($val)) {
			$comparator = $val;
			
			$nt = new NumericToken;
			if ($nt->isValid($tokens[3])) {
				$value = $tokens[3];
				$ft = new FolderItemToken;
				if ($ft->isValid($tokens[4])) {
					$type = $tokens[4];
					return 1;
				} else {
					print ("Invalid folder item type\n");
				}
			} else {
				print ("Invalid numeric value\n");
			}
		} elsif ($ct->isValid("$tokens[2] $tokens[3]")) {
			$comparator = "$tokens[2] $tokens[3]";
			
			$nt = new NumericToken;
			if ($nt->isValid($tokens[4])) {
				$value = $tokens[4];
				$ft = new FolderItemToken;
				if ($ft->isValid($tokens[5])) {
					$type = $tokens[5];
					return 1;
				} else {
					print ("Invalid folder item type\n");
				}
			} else {
				print ("Invalid numeric value\n");
			}
		} else {
			print ("Invalid comparison operator\n");
		}
	} else {
		print ("Not a directory assertion\n");
	}
	
	return 0;
}

sub run {
	
	my ($self, $path, $mask) = @_;
	
	$ft = new FolderItemType;
	
	if (lc($type) eq "item" or lc($type) eq "items") {
		@files = (parse_dir(`ls -al $path`));
		$file_count = @files;
#		print ("item count = $file_count\n");
		
		return $self->compare($file_count, $value);
	
	} elsif (lc($type) eq "file" or lc($type) eq "files") {
	
		$count = 0;
		# count the number of files and links
		foreach $file (parse_dir(`ls -al $path`)) {

			my ($filename, $type, $size, $mtime, $mode) = @$file;
			
			if (-f $filename or -l $filename) {
				$count++;
			}
		}
		
		return $self->compare($count, $value);
		
	
	} elsif (lc($type) eq "folder" or lc($type) eq "folders") {
	
		$count = 0;
	
		# count the number of directories
		foreach $file (parse_dir(`ls -al $path`)) {

			my ($name, $type, $size, $mtime, $mode) = @$file;
			if (-d $filename) {
				if ($filename ne "." and $filename ne "..") {
					$count++;
				}	
			}
		}
		
		return $self->compare($count, $value);
	} 
	
	return 0;
}

sub compare {
	my ($self, $x, $y) = @_;
	
	$comparator = lc($comparator);
#	print("$x $comparator $y\n");
	$ct = new ComparatorToken;

	if (($comparator eq "less than") or ($comparator eq "fewer than") or ($comparator eq '<')) {
		return $x < $y;
	} elsif (($comparator eq "greater than") or ($comparator eq "more than") or ($comparator eq '>')) {
		return $x > $y;
	} elsif (($comparator eq "at most") or ($comparator eq '<=')) {
		return $x <= $y;
	} elsif (($comparator eq "at least") or ($comparator eq '>=')) {
		return $x >= $y;
	} elsif (($comparator eq "exactly") or ($comparator eq "equal to") or ($comparator eq "equals") or ($comparator eq '=')) {
		return $x == $y;
	} else {
		return 0;
	}
}
1;