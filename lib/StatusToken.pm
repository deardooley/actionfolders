#!/usr/local/bin/perl -w
 
package StatusToken;

$EXIST	 			= "exist";
$EXISTS 			= "exists";
$DOES_NOT_EXIST		= "!exist";
$DOES_NOT_EXISTS	= "!exists";
$RUNNING 			= "running";
$QUEUED				= "queued";
$PAUSED 			= "paused";
$STOPPED			= "stopped";

# class that validates numeric tokens

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	return ($s =~ /$QUEUED/i or
			$s =~ /$RUNNING/i or
			$s =~ /$PAUSED/i or
			$s =~ /$STOPPED/i or
			$s =~ /$EXIST/i or 
			$s =~ /$EXISTS/i or 
			$s =~ /$DOES_NOT_EXIST/i or 
			$s =~ /$DOES_NOT_EXISTS/i);
}


1;