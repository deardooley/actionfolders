 #!/usr/local/bin/perl -w
 
package IdentifierToken;


use FileTypeToken;
use AlphaNumericToken;

# class that validates only alphanumeric tokens

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	@tokens = split(/ /, $s);
	$token_count = @tokens;
	
	# a valid statment must have at least 2 tokens
	if ($token_count < 2) {
		return 0;	
	}
	
	$ft = new FileTypeToken;
	if ($ft->isValid($tokens[0])) {
		if ($tokens[1] =~ /named/i) {
			$an = new AlphaNumericToken;
			if ($an->isValid(substr($s,length($tokens[0]) + length($tokens[1]) + 2))) {
				return 1;
			} else {
				print ("Invalid file/folder name\n");
			}
		} else {
			print ("Invalid identifier token\n");
		}
	} else {
		print ("Invalid file type token. Specify 'file' or 'folder'\n");
	}
	return 0;
	
}

1;