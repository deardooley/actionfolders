 #!/usr/local/bin/perl -w
 
package JobCheck;

use IdentifierToken;
use StatusToken;

# class that validates only specifier tokens

$jobid = '';
$status = '';

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	@tokens = split(/ /,$s);
	$token_count = @tokens;
	if ($token_count != 3) {
#		print ("Not a job assertion\n");
		return 0;
	}
	
	if (lc(shift(@tokens)) eq "job") {
		$st = new StatusToken;
		$val = pop(@tokens);
		if ($st->isValid($val)) {
			$status = $val;
			
			$at = new AlphaNumericToken();
			$val = join(' ', @tokens);
			if ($at->isValid($val)) {
				$jobid = $val;
				return 1;
			} else {
				print("Invalid job id\n");
			} 
		} else {
			print("Invalid status\n");
		}
	} else {
		print("Not a job status assertion\n");
	}
	return 0;
}

sub run {
	# exec the system call to query for job status information
	# resulting format is null or similar to:
	# 9:   GPSORBIT.201            408112      byaa705  WAITING
	# because we grep by jobid, there will only be one line returned
	undef $result;
	open (PSTEST, "/Users/dooley/bin/qstat -a | grep $jobid |");
	while (<PSTEST>) {
		$result = $_;
	}
	$st = new StatusToken;
	
	if ($result) {
		## Trim the string
		$result =~ s/^\s+|\s+$//g;
		
		if (lc($status) eq "running") { #
			return $result =~ /RUNNING/i;
		} elsif (lc($status) eq "queued") {
			return $result =~ /WAITING/i;
		} elsif (lc($status) eq "paused") {
			return $result =~ /PAUSED/i;
		} elsif (lc($status) eq "stopped") {
			return $result =~ /STOPPED/i;
		} elsif (lc($status) eq "exist" or lc($status) eq "exists") { # job exists in the queue
			return 1;
		} else {
			return 0;
		} 
	} elsif (lc($status) eq "!exist" or lc($status) eq "!exists") { # no result found, so the job does not exist
		return 1;
	} else {
		return 0;
	}
}
1;