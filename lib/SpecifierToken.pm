 #!/usr/local/bin/perl -w
 
package SpecifierToken;

use IdentifierToken;

# class that validates only specifier tokens

$ANY_FILE = "any file";

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	## Trim the string
	$s =~ s/^\s+|\s+$//g;
	
	$id = new IdentifierToken;
	return ($s =~ /$ANY_FILE/i or $id->isValid($s));
	
}

1;