 #!/usr/local/bin/perl -w
 
package SizeToken;

$BYTES 	= "bytes";
$BYTE	= "byte";
$B 		= "b";
$KB		= "kb";
$MB		= "mb";
$GB		= "gb";
$TB		= "tb";
$PB		= "pb";
$XB		= "xb";

# class that validates numeric tokens

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	return ($s =~ /$B/i or 
			$s =~ /$BYTE/i or 
			$s =~ /$BYTES/i or 
			$s =~ /$KB/i or 
			$s =~ /$MB/i or 
			$s =~ /$GB/i or 
			$s =~ /$TB/i or 
			$s =~ /$PB/i or 
			$s =~ /$XB/i);
	
}

1;