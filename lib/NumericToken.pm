 #!/usr/local/bin/perl -w
 
 package NumericToken;

# class that validates numeric tokens

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	return $s =~ /^[0-9]+$/;
	
}

1;