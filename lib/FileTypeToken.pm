 #!/usr/local/bin/perl -w
 
package FileTypeToken;

$FILE 		= "files";
$FILES		= "file";
$FOLDER 	= "folder";
$FOLDERS	= "folders";

# class that validates numeric tokens

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	return ($s =~ /$FILE/i or 
			$s =~ /$FILES/i or 
			$s =~ /$FOLDER/i or 
			$s =~ /$FOLDERS/i);
}

1;