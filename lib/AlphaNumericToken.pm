#!/usr/local/bin/perl -w
package AlphaNumericToken;

# class that validates only alphanumeric tokens

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my ($self, $s) = @_;
	
	return $s =~ /^[a-zA-Z0-9. *]+$/;
	
}

1;