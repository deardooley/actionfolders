 #!/usr/local/bin/perl -w
 
package AgeCheck;
use File::Listing;
use File::stat;
use Time::Local;

use ComparisonToken;
use AgeToken;
use SpecifierToken;
use NumericToken;

# class that validates only specifier tokens

$name = '';
$comparator = '';
$value = -1;
$timetoken = '';

sub new {
	my ($class_name) = @_;
	
	my ($self) = {};
	
	bless ($self, $class_name);
	
	$self->{'_created'} = 1;
	return $self;	
}

sub isValid {
	my $s = $_[1];
	my @tokens = split(/\s+/,$s);
	my $token_count = @tokens;
	
	#print ("String \"$s\" splits into $token_count tokens\n");
	
	if (!(lc($s) =~ /\s+modified\s+/) or $token_count < 6 or lc($tokens[$token_count -1]) ne 'ago') {
		#print ("Not an age assertion\n");
		return 0;
	}
	
	$tt = new AgeToken;
	pop(@tokens); # 'ago' keyword
	$val = pop(@tokens);
	if ($tt->isValid($val)) {
		$timetoken = $val;
		
		$nt = new NumericToken;
		$val = pop(@tokens);
		if ($nt->isValid($val)) {
			$value = $val;
	
			$ct = new ComparisonToken;
			$val = pop(@tokens);
			if ($ct->isValid($val)) {
				$comparator = $val;
			
				$st = new SpecifierToken;
				pop(@tokens); # 'modified' keyword
				$val = join(' ', @tokens);
				#print ("is specifier $val valid?\n");
				if ($st->isValid($val)) {
					$name = $val;
					if (lc($name) ne "any file") {
						shift(@tokens);
						shift(@tokens);
						$name = join(' ', @tokens);
					}
					return 1;
				} 
			} else {
				$val = pop(@tokens).' '.$val;
				if ($ct->isValid($val)) {
					$comparator = $val;
					
					$st = new SpecifierToken;
					$val = join(' ', @tokens);
					if ($st->isValid($val)) {
						$name = $val;
						if (lc($name) ne "any file") {
							shift(@tokens);
							shift(@tokens);
							$name = join(' ', @tokens);
						}
						return 1;
					} else {
						print ("Invalid specifier string");
					}			
				} else {
					print ("Invalid comparison string");
				}
			}
		} else {
			print ("Invalid age value");
		}
	} else {
		print ("Invalid age token");
	}
	return 0;
}

sub run {
	
	my ($self, $path, $mask) = @_;
	
	$st = new SpecifierToken;
	#print("Specifier name is $name\n");
	
	if (lc($name) eq "any file") { # check to see if any file meets the condition
		#print "this is an \"any file\" case $path\n";
		
		# we must iterate through all files in the folder
		foreach $file (parse_dir(`ls -al $path`)) {
			
			my ($filename, $type, $size, $mtime, $mode) = @$file;
			#print ("checking file $filename\n");
			# resolve the numerical value with the size token ie 2 kb = 2048
			if ($type ne 'd') {
				#print ("file: $filename, $type, $size, $mtime, $mode\n");
				$resolvedval = resolveFileAge($value, $timetoken);
				my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($mtime);
				$year += 1900;
				$mon += 1;
				#print("file $filename last modified on $mon/$mday/$year $hour:$min:$sec\n");
				#print("comparison value is $value $timetoken | $mtime $comparator $resolvedval\n");
				# if this comparison is true, return true, else keep looping
				if (compare($resolvedval, $mtime)) {
					#print ("found one that compared favorably");
					return 1;
				}
			}	
		}
		
		return 0;
		
	} else { # check to see if the named file meets the condition
		if ($name =~ /\*/) {
			print ("grammer name contains wildcard\n");
			foreach $file (parse_dir(`ls -al $path/$name`)) {

				my ($filename, $type, $size, $mtime, $mode) = @$file;
				
				if ($type ne 'd') {
					#print ("file: $filename, $type, $size, $mtime, $mode\n");
					$resolvedval = resolveFileAge($value, $timetoken);
			
					# if this comparison is true, return true, else keep looping
					if (compare($resolvedval, $mtime)) {
						#print ("found one that compared favorably");
						return 1;
					}
				}
			}
            return 0;
            
		} else {
			unless (-e "$path/$name") {
				print ("file does not exist\n");
				return 0;
			}
			
			$time = stat("$path/$name")->mtime;
			
			# resolve the numerical value with the time token ie 1 min = 60
			$resolvedval = resolveFileAge($value, $timetoken);
			#print("comparison value is $value $timetoken | $time $comparator $resolvedval\n");
			# compare the two times according to the defined comparison token
			return compare($resolvedval, $time)
		}
	}
	
}

# convert the value and size token into a single integer value ie 2 kb = 2048
sub resolveFileAge {
	my $val = $_[0];
	my $token = lc($_[1]);
	$token =~ s/^\s+|\s+$//g;
	
	my (undef,undef,undef,$day,$month,$year) = localtime();
	$year += 1900;
	$month += 1;
	
	if ($token eq "year" or $token eq "years") {
		$adjustedseconds = $val * 3600 * 24 * 365; 
	} elsif ($token eq "month" or $token eq "months") {
		my $next_year = ($month == 12) ? $year + 1 : $year;
		my $next_month = timelocal(0, 0, 0, 1, $month % 12, $next_year);
	 	my $days = (localtime($next_month - 86_400))[3];
		
		$adjustedseconds = $val * 3600 * 24 * $days;
		 
	} elsif ($token eq "day" or $token eq "days") {
		$adjustedseconds = $val * 3600 * 24; 
	} elsif ($token eq "hour" or $token eq "hours") {
		$adjustedseconds = $val * 3600; 
	} elsif ($token eq "minute" or $token eq "minutes") {
		$adjustedseconds = $val * 60; 
	} elsif ($token eq "second" or $token eq "seconds") {
		$adjustedseconds = $val;
	}
	
	return time() - $adjustedseconds;
}

sub compare {
	my $x = $_[0];
	my $y = $_[1];
	
	$comparator = lc($comparator);
	
	my (undef,undef,undef,$day,$month,$year) = localtime();
	
	if (($comparator eq "less than") or ($comparator eq "fewer than") or ($comparator eq '<')) {
		return $x < $y;
	} elsif (($comparator eq "greater than") or ($comparator eq "more than") or ($comparator eq '>')) {
		return $x > $y;
	} elsif (($comparator eq "at most") or ($comparator eq '<=')) {
		return $x <= $y;
	} elsif (($comparator eq "at least") or ($comparator eq '>=')) {
		return $x >= $y;
	} elsif (($comparator eq "exactly") or ($comparator eq "equal to") or ($comparator eq "equals") or ($comparator eq '=')) {
		return $x == $y;
	} else {
		return 0;
	}
}
1;